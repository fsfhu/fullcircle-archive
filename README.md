# fullcircle-archive

A Full Circle Magazin magyar fordításának archívuma

# Licenc

Az oldal [CC BY-SA 4.0 licenc](https://creativecommons.org/licenses/by-sa/4.0/) alatt érhető el, az egyes magazinokra azok saját licencei vonatkoznak.

# License

The page is [licensed under CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), the magazine issues are licensed under their respective licenses .

